package ru.home.javarushbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaRushBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaRushBotApplication.class, args);
	}

}
